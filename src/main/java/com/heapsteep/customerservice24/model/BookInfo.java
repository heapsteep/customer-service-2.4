package com.heapsteep.customerservice24.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
@Data
@Setter @Getter
public class BookInfo {
	private String bookTitle;
    private int port;
}