package com.heapsteep.customerservice24.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.heapsteep.customerservice24.model.BookInfo;
import com.heapsteep.customerservice24.proxy.BookServiceProxy;
//import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
//import org.springframework.web.bind.annotation.PathVariable;

@RestController
public class CustomerController {	
	@Autowired
	private BookServiceProxy proxy;
		
	@GetMapping("/getBookDetailFeign/{id}")
	public BookInfo getBookDetailFeign(@PathVariable("id") Long id) {		
		return proxy.getBookDetail(id);		
	}
}
